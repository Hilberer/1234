import UserModel from '../models/User.model.js'
import StatusCode from '../configurations/StatusCode.js'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

const createUser = async (request, response) => {

    const user = new UserModel({
        username: request.body.username,
        password: request.body.password
    })

    const salt = await bcrypt.genSalt(10)
    user.password = await bcrypt.hash(user.password, salt)

    const token = jwt.sign(
        { user_id: user._id },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      )

    user.token = token;

    try {
        const databaseResponse = await user.save()
        response.status(StatusCode.CREATED).send(databaseResponse)

    } catch (error) {
        response.status(StatusCode.INTERNAL_SERVER_ERROR).send({
            message: 'Error while trying to create user',
            stack: error
        })
    }
}

const login = async (request, response) => {

    const user = await UserModel.findOne({ username: request.body.username })

    if (user) {
        const validPassword = await bcrypt.compare(request.body.password, user.password)

        const token = jwt.sign(
            { user_id: user._id },
            process.env.TOKEN_KEY,
            {
              expiresIn: "2h",
            }
          )

          user.token = token

        if (validPassword) {
            try {
                response.status(StatusCode.OK).send({
                    message: user
                })
            } catch (error) {
                response.status(StatusCode.INTERNAL_SERVER_ERROR).send({
                    message: 'Error but user/password correct',
                    stack: error,
                })
            }
          } else {
              try {
                    response.status(StatusCode.OK).send({
                        message: 'Invalid password'
                    })
              } catch (error) {
                  response.status(StatusCode.INTERNAL_SERVER_ERROR).send({
                      message: 'Error but user correct, password wrong',
                      stack: error
                  })
              }
          }
        }   else {
            try {
                response.status(StatusCode.OK).send({
                    message: 'User does not exist'
                })
            } catch (error) {
                response.status(StatusCode.INTERNAL_SERVER_ERROR).send({
                    message: 'Error and user does not exist',
                    stack: error
                })
            }
        }
    }

const createToken = async (request, response) => {

}
const getAllUsers = async (request, response) => {
    try {
        const databaseResponse = await UserModel.find()
        response.status(StatusCode.OK).send(databaseResponse)
    } catch (error) {
        response.status(500).send({ message: error.message })
    }
}

export default {
    createUser,
    getAllUsers,
    login
}