import ProductModel from "../models/Product.model.js"
import StatusCode from "../configurations/StatusCode.js"

const createProduct = async (request, response) => {

    const product = new ProductModel ({
        name: request.body.name,
        desc: request.body.desc,
        price: request.body.price,
        image: request.file.filename
    })
    try {
        const databaseResponse = await product.save()
        response.status(StatusCode.CREATED).send(databaseResponse)

    }   catch (error) {
        response.status(StatusCode.INTERNAL_SERVER_ERROR).send({
            message: 'Error while trying to create product',
            stack: error
        })
    }
}

export default {
    createProduct
}
