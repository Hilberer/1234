import mongoose from 'mongoose'
const {Schema} = mongoose

const userSchema = Schema ({
    username: {
        type: String,
        unique: true,
        allowNull: false,
        required: true
    },

    password: {
        type: String,
        required: true,
        allowNull: false

    },

    token: {
        type: String
    }

},{ timestamps: true })

const UserModel = mongoose.model('user', userSchema)
export default UserModel