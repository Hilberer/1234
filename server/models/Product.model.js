import mongoose from 'mongoose'
const {Schema} = mongoose

const productSchema = Schema ({
    name: {
        type: String,
        allowNull: false,
        required: false
    },
    desc: {
        type: String,
        allowNull: false,
        required: false
    },
    price: {
        type: String,
        allowNull: false,
        required: false
    },
    image: {
        type: String,
        required: false
    }
},{ timestamps: true })

const ProductModel = mongoose.model('product', productSchema)
export default ProductModel
