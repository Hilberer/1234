import ProductController from "../controllers/Product.controller.js"
import multer from "multer"
import path from 'path'


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public')
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + path.extname(file.originalname)
      cb(null, file.fieldname + '-' + uniqueSuffix)
    }
  })

  const upload = multer({ storage: storage })


const routes = (application) => {
    application.post('/product', upload.single('file'), ProductController.createProduct)
}

export default { routes }