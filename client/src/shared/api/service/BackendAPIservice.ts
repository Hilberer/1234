import http from '../BackendAPI'
import { iCreateNewUser } from '../../interface/user'

const createUser = async (data: iCreateNewUser) => {
    try {
        return await http.post('/user', data)
    } catch (error) {
        console.log(error)
    }
    return {data: []}
}
const login = async (data: iCreateNewUser) => {
    try{
        return await http.post('/login', data)
    }   catch (error) {
        console.log(error)
    }
    return {data: []}
}

const getUsers = async () => {
    try{
        return await http.get('/user')
    }   catch(error) {
        console.log(error)
    }
    return {data: []}
}
// eslint-disable-next-line import/no-anonymous-default-export
export default {
    createUser,
    login,
    getUsers
}
