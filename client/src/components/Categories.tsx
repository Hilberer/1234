
import styled from 'styled-components'
import { categories } from '../shared/data'
import { CategoryItem } from './CategoryItem'

const Container = styled.div`
    display: flex;
    padding: 20px;
    justify-content: space-between;
`
export const Categories = () => {
    return (
        <Container>
            {categories.map(item => (

                <CategoryItem key={item.id} img={item.img} title={item.title} id={item.id}/>
            ))}
        </Container>
    )
}
