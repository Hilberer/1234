import styled from 'styled-components'
import { FavoriteBorderOutlined } from '@material-ui/icons'
import telefon from '../shared/images/telefon.jpg'

const Container = styled.div`
    width: 200px;
    height: 300px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-color: #f5f6f7;
    position: relative;
    padding: 10px;
    margin: 10px;
    border: 1px solid grey;
    border-radius: 5px;
    &:hover{
        box-shadow: 0 0 11px rgba(33,33,33,.2);
    }
`
const Button = styled.button`
    background-color: #04AA6D;
    color: white;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    width: 80%;
    height: 30px;
    margin-bottom:5px;
    font-size: 12px;
    font-weight: bold;
    &:hover{
        opacity: 0.8;
    }
`
const Image = styled.img`
    width: 100%;
    height: 100%;
    object-fit: cover;
`
const Info = styled.div`
`
const Price = styled.div`
`
const Icon = styled.div`
    position: absolute;
    right: 10px;
    top: 10px;
`

export const ProductCard = () => {
  return (
      <Container>
          <Image src={telefon} alt={''}/>
          <Icon><FavoriteBorderOutlined/></Icon>
          <Info>test hej</Info>
          <Price>1254kr</Price>
          <Button>Köp</Button>
      </Container>
  )
}
