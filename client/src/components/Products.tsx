import styled from 'styled-components'
import { popularProducts } from '../shared/data'
import { Product } from './Product'

const Container = styled.div`
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`

export const Products = () => {
    return (
        <Container>
            {popularProducts.map((item) => (
                <Product img={item.img} key={item.id} id={item.id} />
            ))}
        </Container>
    )
}
