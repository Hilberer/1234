import styled from 'styled-components'

const Form = styled.form`
    max-width: 300px;
    padding: 10px;
    background-color: white;
    border: 1px solid grey;
`
const Input = styled.input`
    width: 100%;
    padding-top: 10px;
    padding-bottom: 10px;
    margin-bottom: 10px;
    border: none;
    background: #f1f1f1;
    :focus {
        background-color: #ddd;
        outline: none;
    }
`
const Label = styled.label`
`
export const UploadForm = () => {
  return (
    <Form action="/upload" method="POST" encType="multipart/form-data">
        <Input type="text" placeholder="Name" name="name"/>
        <Input type="text" placeholder="Description" name="disc"/>
        <Input type="text" placeholder="Price" name="pris"/>
        <Label>Picture</Label>
        <Input type="file" name="file" id="file"/>
        <Label>test</Label>
        <Input type="submit" value="submit" />
    </Form>
  )
}
