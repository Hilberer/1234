
import React from 'react'
import styled from 'styled-components'
import SearchIcon from '@material-ui/icons/Search';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import { Badge } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom'
import RoutingPath from '../routes/RoutingPath'


const Container = styled.div`
    height: 60px;
`;
const Wrapper = styled.div`
    padding: 10px 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

const Left = styled.div`
    flex:1;
    display: flex;
`;

const Center = styled.div`
    flex:2;
    text-align: center;
`;

const Right = styled.div`
    flex:1;
    display: flex;
    align-items: center;
    justify-content: flex-end;
`;

const SearchContainer = styled.div`
    border: 0.5px solid lightgray;
    display: flex;
    align-items: center;
    margin-left: 25px;
    padding: 5px;
`;

const Input = styled.input`
    border: none;
`

const Logo = styled.h1`
    font-weight: bold;
`
const MenuItem = styled.div`
    font-size: 14px;
    margin-left: 25px;
    cursor: pointer;
`

export const Navbar = () => {

    return (
        <Container>
            <Wrapper>
                <Left>
                    <SearchContainer>
                        <Input/>
                        <SearchIcon style={{color:'gray', fontSize:16}}/>
                    </SearchContainer>
                </Left>
                <Center>
                    <Logo>HEJ</Logo>
                </Center>
                <Right>
                    <MenuItem as={RouterLink} to={RoutingPath.register}>REGISTER</MenuItem>
                    <MenuItem as={RouterLink} to={RoutingPath.signin}>SIGNIN</MenuItem>
                    <MenuItem as={RouterLink} to={RoutingPath.home}>
                        <Badge badgeContent={4} color="primary">
                            <ShoppingCartOutlinedIcon/>
                        </Badge>
                    </MenuItem>
                </Right>
            </Wrapper>
        </Container>
    )
}
