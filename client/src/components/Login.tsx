import styled from 'styled-components'
import BackendAPIservice from '../shared/api/service/BackendAPIservice'
import { useState, useEffect } from 'react'
import { iCreateNewUser } from '../shared/interface/user'


const Form = styled.form`
    max-width: 300px;
    padding: 10px;
    background-color: white;
    border: 1px solid grey;
`
const Input = styled.input`
    width: 100%;
    padding-top: 10px;
    padding-bottom: 10px;
    margin-bottom: 10px;
    border: none;
    background: #f1f1f1;
    :focus {
        background-color: #ddd;
        outline: none;
    }
`
const Label = styled.label`
`
const Button = styled.button`
    background-color: #04AA6D;
    color: white;
    padding: 16px 20px;
    border: none;
    cursor: pointer;
    width: 100%;
    margin-bottom:10px;
    opacity: 0.8;
`

export const Login = () => {

    const [newUser, setNewUser] = useState<iCreateNewUser> ({
        username: 'naaaa',
        password: 'secret'
    })
    const createUser = async () => {
        console.log(newUser)
        try {
           const response = BackendAPIservice.createUser(newUser)
           console.log(response)

        }   catch (error) {
                console.log(error)
        }
    }
    const user = async () => {
        try {
            const response = BackendAPIservice.login(newUser)
            console.log(response)
        }   catch (error) {
                console.log(error)
        }
    }
    useEffect(() => {
        console.log(newUser)
    },[newUser])

  return (
    <Form>
        <Label>Username</Label>
        <Input type="text" placeholder="Enter username" name="username" required onChange={(event) => setNewUser({ ...newUser, username: event.target.value})}/>
        <Label>Password</Label>
        <Input type="text" placeholder="Enter Password" name="password" required onChange={(event) => setNewUser({...newUser, password: event.target.value})}/>
        <Button type="button" onClick={() => user()}>Login</Button>
        <Button type="button" onClick={() => createUser()}>Register</Button>
    </Form>
  )
}
