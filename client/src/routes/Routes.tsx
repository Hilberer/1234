import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import RoutingPath from './RoutingPath'
import { Home } from '../views/Home'
import { About } from '../views/About'
import { Register } from '../views/Register'
import { Signin } from '../views/Signin'

export const Routes = (props: { children: React.ReactChild }) => {
    const { children } = props
    return (
        <BrowserRouter>
            {children}
            <Switch>
                <Route exact path={RoutingPath.home} component={Home} />
                <Route exact path={RoutingPath.about} component={About} />
                <Route exact path={RoutingPath.register} component={Register} />
                <Route exact path={RoutingPath.signin} component={Signin} />
                <Route component={Home} />
            </Switch>
        </BrowserRouter>
    )
}
