const home = '/'
const about = '/about'
const register = '/register'
const signin = '/signin'

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    home,
    about,
    register,
    signin

}