
import { Navbar } from "./components/Navbar";
import { Routes } from './routes/Routes'


function App() {
  return (
    <div className="App">
      <Routes>
        <Navbar/>
      </Routes>
    </div>
  );
}

export default App;
