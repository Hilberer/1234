import React from 'react'
import { Categories } from '../components/Categories'
import { Products } from '../components/Products'

export const Home = () => {
    return (
        <div>
            <Categories />
            <Products />
        </div>
    )
}
