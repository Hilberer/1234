import styled from 'styled-components'
import { ProductCard } from './../components/ProductCard'
import { UploadForm } from '../components/UploadForm'

const Container = styled.div`
    display: flex;
    flex-wrap: wrap;
    margin: 20px;
`
export const Register = () => {
    return (
        <Container>
            <ProductCard />
            <ProductCard />
            <UploadForm />
        </Container>
    )
}
